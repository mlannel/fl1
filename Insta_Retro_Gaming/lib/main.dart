import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
// import 'package:flutter_app_test/screens/Menu/pictures/viewPicture.dart';
import 'package:flutter_app_test/screens/auth/login_page.dart';
import 'package:flutter_app_test/screens/auth/signin_page.dart';
import 'package:flutter_app_test/screens/Menu/profile.dart';
import 'package:flutter_app_test/services/auth.dart';
import 'package:provider/provider.dart';
import 'package:flutter_app_test/screens/wrapper.dart';
import 'models/user.dart';

void main() {
  runApp(
    MyApp(),
  );
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final Map<String, Widget Function(BuildContext context)> map =
        <String, Widget Function(BuildContext context)>{
      '/login': (BuildContext context) => LogIn(),
      // '/upload': (BuildContext context) => ViewPicture(),
      '/profile': (BuildContext context) => Userinfo(),
      '/signin': (BuildContext context) => SignIn(),
    };

    return StreamProvider<User>.value(
      value: AuthService().user,
      child: MaterialApp(
        title: 'Insta Retro Gaming',
        theme: ThemeData(
          brightness: Brightness.light,
          backgroundColor: Colors.purpleAccent,
          primaryColor: Colors.deepPurpleAccent,
          accentColor: Colors.cyanAccent,
          buttonColor: Colors.deepPurpleAccent,
          fontFamily: 'PermanentMarker',
          visualDensity: VisualDensity.adaptivePlatformDensity,
          textTheme: const TextTheme(
            headline1: TextStyle(fontSize: 72.0, fontWeight: FontWeight.bold),
            headline6: TextStyle(fontSize: 36.0, fontStyle: FontStyle.italic),
            bodyText2: TextStyle(fontSize: 14.0, fontFamily: 'Hind'),
          ),
        ),
        debugShowCheckedModeBanner: false,
        home: Wrapper(),
        routes: map,
      ),
    );
  }
}
