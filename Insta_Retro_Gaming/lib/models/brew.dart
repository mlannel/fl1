class Brew {
  Brew({this.email, this.name, this.urlPic});

  final String email;
  final String name;
  final String urlPic;
}
