// import 'package:firebase_auth/firebase_auth.dart';
// FirebaseUser actual;

class User {
  User({this.uid});
  final String uid;
}

class UserData {
  UserData({this.uid, this.email, this.name, this.urlPic});

  final String uid;
  final String email;
  final String name;
  final String urlPic;
}
