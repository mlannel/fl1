import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_app_test/models/user.dart';
import 'package:flutter_app_test/services/database.dart';
import 'package:google_sign_in/google_sign_in.dart';

class AuthService {
  final FirebaseAuth _oth = FirebaseAuth.instance;
  final GoogleSignIn googleSignIn = GoogleSignIn();

  User _userFromFirebaseUser(FirebaseUser user) {
    if (user != null) {
      return User(uid: user.uid);
    } else {
      return null;
    }
  }

  // authchange user stream
  Stream<User> get user {
    return _oth.onAuthStateChanged
        //.map((FirebaseUser user) => _userFromFirebaseUser(user));
        .map(_userFromFirebaseUser);
  }

  //sign in email
  Future<User> signInWithEmailAndPassword(String email, String password) async {
    try {
      final AuthResult result = await _oth.signInWithEmailAndPassword(
          email: email, password: password);
      final FirebaseUser user = result.user;
      return _userFromFirebaseUser(user);
    } catch (e) {
      print(e.toString());
      return null;
    }
  }

  //register
  Future<User> registerWithEmailAndPassword(
      String email, String password) async {
    try {
      final AuthResult result = await _oth.createUserWithEmailAndPassword(
          email: email, password: password);
      final FirebaseUser user = result.user;
      //
      await DatabaseService(uid: user.uid).updateUserData(
          'unknow@email.com',
          'unknow Name',
          'http://omarseillais.e-monsite.com/medias/site/logos/images-3.jpg');
      return _userFromFirebaseUser(user);
    } catch (e) {
      print(e.toString());
      return null;
    }
  }

  // sign in with google

  Future<User> googleSignInA() async {
    try {
      final GoogleSignInAccount googleSignInAccount =
          await googleSignIn.signIn();

      if (googleSignInAccount != null) {
        final GoogleSignInAuthentication googleSignInAuthentication =
            await googleSignInAccount.authentication;

        final AuthCredential credential = GoogleAuthProvider.getCredential(
            idToken: googleSignInAuthentication.idToken,
            accessToken: googleSignInAuthentication.accessToken);

        // ignore: unused_local_variable
        final AuthResult result = await _oth.signInWithCredential(credential);
        final FirebaseUser user = await _oth.currentUser();

        await DatabaseService(uid: user.uid)
            .updateUserData(user.email, user.displayName, user.photoUrl);

        // actual = user;

        return _userFromFirebaseUser(user);
      }
    } catch (e) {
      print(e.toString());
      return null;
    }
    return null;
  }

  // Future<FirebaseUser> getInfoUser() async {
  //   try {
  //     return _oth.currentUser();
  //   } catch (e) {
  //     print(e.toString());
  //     return null;
  //   }
  // }

  //sign out
  Future<void> signOut() async {
    try {
      // actual = null;
      return await _oth.signOut();
    } catch (e) {
      print(e.toString());
      // return null;
    }
  }
}
