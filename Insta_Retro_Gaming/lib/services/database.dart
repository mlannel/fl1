import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter_app_test/models/brew.dart';
import 'package:flutter_app_test/models/user.dart';

class DatabaseService {
  DatabaseService({this.uid});

  final String uid;

  //collection reference

  final CollectionReference brewCollection =
      Firestore.instance.collection('brews');

  Future<void> updateUserData(String email, String name, String urlPic) async {
    final Map<String, String> data = <String, String>{
      'email': email,
      'name': name,
      'urlPic': urlPic,
    };
    return await brewCollection.document(uid).setData(data);
  }

  List<Brew> _brewListFromSnapshot(QuerySnapshot snapshot) {
    return snapshot.documents.map((DocumentSnapshot doc) {
      return Brew(
        name: doc.data['name'] as String ?? '',
        email: doc.data['email'] as String ?? '',
        urlPic: doc.data['urlPic'] as String ?? '',
      );
    }).toList();
  }

  UserData _userDataFromSnapshot(DocumentSnapshot snapshot) {
    return UserData(
        uid: uid,
        email: snapshot.data['email'] as String,
        name: snapshot.data['name'] as String,
        urlPic: snapshot.data['urlPic'] as String);
  }

  //get brews streams
  Stream<List<Brew>> get brews {
    return brewCollection.snapshots().map(_brewListFromSnapshot);
  }

  Stream<UserData> get userData {
    return brewCollection.document(uid).snapshots().map(_userDataFromSnapshot);
  }
}
