import 'package:flutter/material.dart';
import 'package:flutter_app_test/models/user.dart';
import 'package:flutter_app_test/screens/Menu/log_menu.dart';
import 'package:provider/provider.dart';

import 'auth/login_page.dart';
// import 'singed/logmenu.dart';

class Wrapper extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final User user = Provider.of<User>(context);
    print(user);

    //return home or auth widget
    if (user == null) {
      return LogIn();
    } else {
      return Menu();
    }
  }
}
