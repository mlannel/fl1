import 'package:flutter/material.dart';
import 'package:flutter_app_test/services/auth.dart';
import 'pictures/view_picture.dart';

class Menu extends StatefulWidget {
  @override
  _MyMenu createState() => _MyMenu();
}

class _MyMenu extends State<Menu> {
  final AuthService _auth = AuthService();
//4
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          actions: <Widget>[
            IconButton(
              icon: const Icon(
                Icons.account_box,
                size: 30,
              ),
              onPressed: () => Navigator.pushNamed(context, '/profile'),
              color: Colors.cyanAccent,
            ),
            Builder(builder: (BuildContext context) {
//5
              return IconButton(
                icon: const Icon(
                  Icons.power_settings_new,
                  size: 30,
                ),
                onPressed: () async {
                  await _auth.signOut();
                },
                color: Colors.red,
              );
            })
          ],
        ),
        body: Container(
          child: ViewPicture(),
        ));
  }
}
