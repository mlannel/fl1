import 'dart:io';
import 'dart:math';

import 'package:firebase_storage/firebase_storage.dart';
import 'package:image_picker/image_picker.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

  Future<void> getImage(ImageSource source) async {
    final File image = await ImagePicker.pickImage(source: source);
    _uploadImageToFirebase(image);
  }

  Future<void> _uploadImageToFirebase(File image) async {
    try {
      // Make random image name.
      final int randomNumber = Random().nextInt(100000);
      final String imageLocation = 'images/image$randomNumber.jpg';

      // Upload image to firebase.
      final StorageReference storageReference = FirebaseStorage().ref().child(imageLocation);
      final StorageUploadTask uploadTask = storageReference.putFile(image);
      await uploadTask.onComplete;
      _addPathToDatabase(imageLocation);
    }catch(e){
      print(e.message);
    }
  }

  Future<void> _addPathToDatabase(String text) async {
    try {
      // Get image URL from firebase
      final StorageReference ref = FirebaseStorage().ref().child(text);
      final String imageString = await ref.getDownloadURL() as String;

      // Add location and url to database
      await Firestore.instance.collection('storage').document().setData(<String, dynamic>{'url':imageString , 'location':text});
    }catch(e){
      print(e.message);
    }
  }

class Record {
  Record.fromMap(Map<String, dynamic> map, {this.reference})
      : assert(map['location'] != null),
        assert(map['url'] != null),
        location = map['location'] as String,
        url = map['url'] as String;

  Record.fromSnapshot(DocumentSnapshot snapshot)
      : this.fromMap(snapshot.data, reference: snapshot.reference);

  final String location;
  final String url;
  final DocumentReference reference;

  @override
  String toString() => 'Record<$location:$url>';
}