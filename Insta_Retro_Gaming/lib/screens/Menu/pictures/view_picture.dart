import 'package:flutter/material.dart';
import 'package:flutter_app_test/screens/Menu/pictures/upload_picture.dart';
import 'package:flutter_app_test/screens/Menu/pictures/get_picture.dart';
import 'package:image_picker/image_picker.dart';

class ViewPicture extends StatefulWidget {
  @override _ViewPicture createState() => _ViewPicture();
}

class _ViewPicture extends State<ViewPicture> {

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async => false,
    child: Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Flexible(
                child: buildBody(context),
            ),
          ],
        ),
      ),
      bottomNavigationBar: BottomAppBar(
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            IconButton(
              icon: const Icon(
                Icons.photo_camera,
                size: 30,
              ),
              onPressed: () => getImage(ImageSource.camera),
              color: Colors.blue,
            ),
            IconButton(
              icon: const Icon(
                Icons.photo_library,
                size: 30,
              ),
              onPressed: () => getImage(ImageSource.gallery),
              color: Colors.pink,
            ),
          ],
        ),
      ),
    )
    );
  }
}