import 'package:flutter/material.dart';
// import 'package:flutter_app_test/models/brew.dart';
import 'package:flutter_app_test/models/user.dart';
// import 'package:flutter_app_test/models/user.dart';
import 'package:flutter_app_test/services/database.dart';
import 'package:provider/provider.dart';
// import 'brew_list.dart';

class Userinfo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final User user = Provider.of<User>(context);
    return StreamBuilder<UserData>(
        stream: DatabaseService(uid: user.uid).userData,
        builder: (BuildContext context, AsyncSnapshot<UserData> snapshot) {
          if (snapshot.hasData) {
            final UserData userData = snapshot.data;
            return Scaffold(
              body: Center(
                child: SafeArea(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Text(
                        userData.email,
                      ),
                      Image.network(userData.urlPic),
                      const Text('your name is :'),
                      Text(userData.name),
                      const SizedBox(
                        height: 12.0,
                      ),
                      IconButton(
                        icon: const Icon(
                          Icons.arrow_back,
                          size: 40,
                        ),
                        onPressed: () => Navigator.of(context).pop(),
                        color: Colors.red,
                      ),
                    ],
                  ),
                ),
              ),
            );
          } else {
            return const Center(
              child: Text('No data found.'),
            );
          }
        });
    // return StreamProvider<List<Brew>>.value(
    //   value: DatabaseService().brews,

    //   child: BrewList(),
// (

//         child: SafeArea(

//           child: Column(
//             mainAxisAlignment: MainAxisAlignment.center,
//             crossAxisAlignment: CrossAxisAlignment.center,
//             children: <Widget>[
//             ],
//           ),
//         ),

//       ),
    // return Scaffold(
    //   body: Center(
    //     child: SafeArea(
    //       child: Column(
    //         mainAxisAlignment: MainAxisAlignment.center,
    //         crossAxisAlignment: CrossAxisAlignment.center,
    //         children: <Widget>[
    //           Text(actual != null
    //               ? (actual.displayName)
    //               : 'reserved for google member'),
    //           Image.network(actual != null
    //               ? (actual.photoUrl)
    //               : 'http://omarseillais.e-monsite.com/medias/site/logos/images-3.jpg'),
    //           const Text('your name is :'),
    //           Text(actual != null ? (actual.displayName) : 'none'),
    //           Text(actual != null ? (actual.email) : 'none'),
    //           const SizedBox(
    //             height: 12.0,
    //           ),
    //           IconButton(
    //             icon: const Icon(
    //               Icons.arrow_back,
    //               size: 30,
    //             ),
    //             onPressed: () => Navigator.of(context).pop(),
    //             color: Colors.red,
    //           ),
    //         ],
    //       ),
    //     ),
    //   ),
    // );
  }
}
