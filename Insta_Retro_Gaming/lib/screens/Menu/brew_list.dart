import 'package:flutter/material.dart';
import 'package:flutter_app_test/models/brew.dart';
import 'package:provider/provider.dart';
import 'brew_tile.dart';

class BrewList extends StatefulWidget {
  @override
  _BrewListState createState() => _BrewListState();
}

class _BrewListState extends State<BrewList> {
  @override
  Widget build(BuildContext context) {
    final List<Brew> brews = Provider.of<List<Brew>>(context);

    int itemCounter = 0;
    if (brews != null) {
      if (brews.length != null) {
        itemCounter = brews.length;
      }
    }
    return ListView.builder(
      itemCount: itemCounter,
      itemBuilder: (BuildContext context, int index) {
        return BrewTile(brew: brews[index]);
      },
    );
  }
}
