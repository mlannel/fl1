import 'package:flutter/material.dart';
import 'package:flutter_app_test/models/brew.dart';

class BrewTile extends StatelessWidget {
  const BrewTile({this.brew});

  final Brew brew;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 8.0),
      child: Card(
        margin: const EdgeInsets.fromLTRB(20, 6, 20, 0),
        child: ListTile(
          leading: const CircleAvatar(
            radius: 25.0,
            backgroundColor: Colors.purple,
          ),
          title: Text(brew.name),
          subtitle: Text(brew.email),
        ),
      ),
    );
  }
}
