import 'package:flutter/material.dart';
import 'package:flutter_app_test/services/auth.dart';

class RegisterEmailSection extends StatefulWidget {
  final String title = 'Registration';
  @override
  State<StatefulWidget> createState() => RegisterEmailSectionState();
}

class RegisterEmailSectionState extends State<RegisterEmailSection> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final AuthService _oth = AuthService();

  String email = '';
  String password = '';
  String error = '';

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          const SizedBox(height: 20.0),
          TextFormField(
            onChanged: (String val) {
              setState(() => email = val);
            },
            // controller: _emailController,
            decoration: const InputDecoration(labelText: 'Email'),
            validator: (String value) {
              if (value.isEmpty) {
                return 'Please enter an email';
              }
              return null;
            },
          ),
          TextFormField(
            obscureText: true,
            onChanged: (String val) {
              setState(() => password = val);
            },
            // controller: _passwordController,
            decoration: const InputDecoration(labelText: 'Password'),
            validator: (String val) {
              if (val.length < 6) {
                return 'Please enter a password 6+ chars long';
              }
              return null;
            },
          ),
          const SizedBox(
            height: 12.0,
          ),
          Text(
            error,
            style: const TextStyle(
              color: Colors.black,
              fontSize: 14,
            ),
          ),
          const SizedBox(
            height: 12.0,
          ),
          Container(
            padding: const EdgeInsets.symmetric(vertical: 16.0),
            alignment: Alignment.center,
            child: RaisedButton(
              onPressed: () async {
                if (_formKey.currentState.validate()) {
                  print(email);
                  print(password);
                  final dynamic result =
                      await _oth.registerWithEmailAndPassword(email, password);
                  if (result == null) {
                    setState(() {
                      error = 'not valid';
                    });
                  } else {
                    Navigator.of(context).pop();
                  }
                }
              },
              child: const Text('Sign in'),
            ),
          ),
        ],
      ),
    );
  }
}
