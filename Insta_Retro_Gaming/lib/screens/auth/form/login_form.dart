import 'package:flutter/material.dart';
import 'package:flutter_app_test/services/auth.dart';

class EmailPasswordForm extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => EmailPasswordFormState();
}

class EmailPasswordFormState extends State<EmailPasswordForm> {
  final AuthService _oth = AuthService();

  String email = '';
  String password = '';
  String error = '';

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          const SizedBox(height: 20.0),
          TextFormField(
            onChanged: (String val) {
              setState(() => email = val);
            },
            decoration: const InputDecoration(labelText: 'Email'),
            validator: (String val) {
              if (val.isEmpty) {
                return 'Please enter an email';
              }
              return null;
            },
          ),
          TextFormField(
            obscureText: true,
            onChanged: (String val) {
              setState(() => password = val);
            },
            decoration: const InputDecoration(labelText: 'Password'),
            validator: (String value) {
              if (value.isEmpty) {
                return 'Please enter password';
              }
              return null;
            },
          ),
          const SizedBox(
            height: 12.0,
          ),
          Text(
            error,
            style: const TextStyle(
              color: Colors.black,
              fontSize: 14,
            ),
          ),
          const SizedBox(
            height: 12.0,
          ),
          Container(
            padding: const EdgeInsets.symmetric(vertical: 16.0),
            alignment: Alignment.center,
            child: RaisedButton(
              onPressed: () async {
                if (_formKey.currentState.validate()) {
                  print(email);
                  print(password);
                  final dynamic result =
                      await _oth.signInWithEmailAndPassword(email, password);
                  if (result == null) {
                    setState(() {
                      error = 'Could not sign in with those cedentials';
                    });
                  }
                }
              },
              child: const Text('Log in'),
            ),
          ),
        ],
      ),
    );
  }
}
