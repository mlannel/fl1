import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_app_test/screens/auth/form/login_form.dart';
import 'package:flutter_app_test/services/auth.dart';

String img2 = 'assets/logo.jpg';

class LogIn extends StatefulWidget {
  @override
  _LogIn createState() => _LogIn();
}

class _LogIn extends State<LogIn> {
  final AuthService _oth = AuthService();
  String error = '';

  @override
  Widget build(BuildContext context) {
    final List<Shadow> shadows2 = <Shadow>[
      const Shadow(
        offset: Offset(10.0, 10.0),
        blurRadius: 3.0,
        color: Color.fromARGB(255, 0, 0, 0),
      ),
      const Shadow(
        offset: Offset(10.0, 10.0),
        blurRadius: 8.0,
        color: Color.fromARGB(125, 0, 0, 255),
      ),
    ];

    return Scaffold(
      backgroundColor: Theme.of(context).backgroundColor,
      resizeToAvoidBottomPadding: false,
      body: Center(
        child: SafeArea(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              CircleAvatar(
                backgroundColor: Theme.of(context).accentColor,
                radius: 70,
                child: Image(image: AssetImage(img2)),
              ),
              Text(
                'Insta Retro Gaming',
                style: TextStyle(
                  color: Theme.of(context).accentColor,
                  fontSize: 35,
                  fontWeight: FontWeight.bold,
                  shadows: shadows2,
                ),
              ),
              SizedBox(
                height: 20,
                width: 100,
                child: Divider(
                  color: Colors.teal.shade100,
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  RaisedButton(
                    color: Theme.of(context).backgroundColor,
                    onPressed: () async {
                      final dynamic result = await _oth.googleSignInA();
                      if (result == null) {
                        setState(() {
                          error = 'Could not sign in with those credentials';
                        });
                      }
                    },
                    child: CircleAvatar(
                      backgroundColor: Theme.of(context).backgroundColor,
                      radius: 50,
                      child: const Image(
                        image: AssetImage('assets/téléchargement.png'),
                      ),
                    ),
                  ),
                ],
              ),
              const SizedBox(
                height: 12.0,
              ),
              Text(
                error,
                style: const TextStyle(
                  color: Colors.white,
                  fontSize: 14,
                ),
              ),
              EmailPasswordForm(),
              RaisedButton(
                color: Theme.of(context).buttonColor,
                child: Text(
                  'Register',
                  style: TextStyle(
                    color: Theme.of(context).accentColor,
                    fontSize: 18,
                    fontFamily: 'Anton',
                  ),
                ),
                onPressed: () {
                  Navigator.pushNamed(context, '/signin');
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}
