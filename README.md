## FL1 - Insta_Retro_Gaming

# How to build APK:

-Download Visual Studio code to build the apk (https://code.visualstudio.com/download)
-Install the following extension in Visual Studio Code:
    -Dart 3.16.0
    -Flutter 3.16.0

-Open the folder where the project Insta_Retro_Gaming is saved

-Open a terminal in VSCode (ctrl + shift + ù)
-Execute the command "flutter packages get"
-Connect your device or create an emulator
-Then execute the command "flutter run" or press "F5" in VSCode
-And here you are, you have our application on your device

# Build app.release.apk for android

-Execute the command "flutter build apk --release"
-Then you can find the .apk in folder : "FL1\fl1\Insta_Retro_Gaming\build\app\outputs\flutter-apk"

# Build app.release.apk for IOS (On macOs only)

-Execute the command "flutter build ios --release"
-Then you can find the .apk in folder : "FL1\fl1\Insta_Retro_Gaming\build\app\outputs\flutter-apk"

# How to log in with google

-We need to add your SHA certificate into firebase first, without it, you won't be able to log in with your google account
-Generate your SHA : 
    -For Windows (keytool -list -v \ -alias androiddebugkey -keystore %USERPROFILE%.android\debug.keystore)
    -For Linux / MacOs (keytool -list -v \ -alias androiddebugkey -keystore ~/.android/debug.keystore)
-Open this link: "https://console.firebase.google.com/u/0/project/insta-retro-gaming-1bdb1/settings/general/android:com.example.flutter_app_test"
-Click on add a fingerprint and type your SHA-1


# Download .apk via CodeMagic

-You can download the .apk directly via the following link: https://codemagic.io/app/5fa68c0f97db291b24a381ea/build/5fa81cf329657a000e9740c8
-Download the file "app-debug.apk"
-Move it to your device
-Execute it